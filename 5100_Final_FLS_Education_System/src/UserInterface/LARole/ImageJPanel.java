/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.LARole;

import Business.EcoSystem;

import Business.Enterprise.Enterprise;
import Business.Network.Network;

import Business.UserAccount.UserAccount;
import Business.WorkQueue.LocalRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author qiufeng
 */
public class ImageJPanel extends javax.swing.JPanel {

    /**
     * Creates new form ImageJPanel
     */
    JPanel jp;
    UserAccount Transaccount;
    WorkRequest wr;
    Enterprise enterprise;
    EcoSystem s;

    ImageJPanel(final JPanel jp, Enterprise enterprise, EcoSystem system) {

        this.jp = jp;
        this.enterprise = enterprise;
        this.s = system;
        // this.wr = wr;
        initComponents();
        // upc.removeAll();
        // jp.setLayout(null);
        jp.setPreferredSize(new Dimension(100, 100));
        final JPanel n = new ChartPanel(CreatePieJPanel());
        jp.add(n);
        final JButton newButton = new JButton("<Back");
        newButton.setPreferredSize(new java.awt.Dimension(33, 33));
        jp.add(newButton);
        newButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jp.remove(n);
                jp.remove(1);
                CardLayout layout = (CardLayout) jp.getLayout();

                layout.previous(jp);
                newButton.setSize(0, 0);
                jp.remove(newButton);
                //                CardLayout layout1 = (CardLayout) jp.getLayout();

                //layout1.previous(jp);
                System.out.println("触发了事件");
            }
        });

        //jPanel1.setPreferredSize(new Dimension(10,10));
        //newButton.setSize(10, 10);
    }

    JFreeChart CreatePieJPanel() {
        int n = 0, e = 0, m = 0, a = 0, g = 0;

        DefaultPieDataset dfp = new DefaultPieDataset();
        for (int i = 0; i < enterprise.getWorkQueue().getWorkRequestList().size(); i++) {
            if (enterprise.getWorkQueue().getWorkRequestList().get(i).getSuject().equals("Math")) {
                n++;
            }
            if (enterprise.getWorkQueue().getWorkRequestList().get(i).getSuject().equals("English")) {
                e++;
            }
            if (enterprise.getWorkQueue().getWorkRequestList().get(i).getSuject().equals("Science")) {
                m++;
            }
            if (enterprise.getWorkQueue().getWorkRequestList().get(i).getSuject().equals("Physics")) {
                a++;
            }
            if (enterprise.getWorkQueue().getWorkRequestList().get(i).getSuject().equals("Chineses")) {
                g++;
            }
        }
        for (int j = 0; j < s.getNetworkList().size(); j++) {
            for (int k = 0; k < s.getNetworkList().get(j).getEnterpriseDirectory().getEnterpriseList().size(); k++) {
                if (s.getNetworkList().get(j).getEnterpriseDirectory().getEnterpriseList().get(k).getEnterpriseType().getValue().equals("Localization Company")) {
                    for (int l = 0; l < s.getNetworkList().get(j).getEnterpriseDirectory().getEnterpriseList().get(k).getWorkQueue().getWorkRequestList().size(); l++) {
                        LocalRequest lr = (LocalRequest) s.getNetworkList().get(j).getEnterpriseDirectory().getEnterpriseList().get(k).getWorkQueue().getWorkRequestList().get(l);
                        if (lr != null && lr.getOnlineEnterprise() != null && lr.getOnlineEnterprise().equals(enterprise.getName())) {
                            for (int h = 0; h < enterprise.getWorkQueue().getWorkRequestList().size(); h++) {
                                if (lr.getSuject().equals("Math")) {
                                    n++;
                                }
                                if (lr.getSuject().equals("English")) {
                                    e++;
                                }
                                if (lr.getSuject().equals("Science")) {
                                    m++;
                                }
                                if (lr.getSuject().equals("Physics")) {
                                    a++;
                                }
                                if (lr.getSuject().equals("Chineses")) {
                                    g++;
                                }
                            }
                        }
                    }
                }
            }

        }
        ;
        dfp.setValue("Math", n);
        dfp.setValue("English", e);
        dfp.setValue("Science", m);
        dfp.setValue("Physics", a);
        dfp.setValue("Chineses", g);
        //Create JFreeChart object
        JFreeChart chart = ChartFactory.createPieChart("Statistic of Subject", dfp, true, true, true);

        return chart;

    }

    JFreeChart CreateLineJPanel() {
        DefaultCategoryDataset dfp = new DefaultCategoryDataset();
        dfp.addValue(212.0, "First", "1001.0");
        dfp.addValue(504.0, "First", "1001.1");
        dfp.addValue(1520.0, "First", "1001.2");

        dfp.addValue(712.0, "Second", "1001.0");
        dfp.addValue(1204.0, "Second", "1001.1");
        dfp.addValue(1820.0, "Second", "1001.2");
        //Create JFreeChart object
        JFreeChart a = ChartFactory.createLineChart("jfreechart test",// 图表标题  
                "X", // 主轴标签（x轴）  
                "Y",// 范围轴标签（y轴）  
                dfp);

        return a;

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();

        jButton1.setText("Back");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton1)
                .addContainerGap(649, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton1)
                .addContainerGap(285, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        jp.remove(this);
        CardLayout layout = (CardLayout) jp.getLayout();
        layout.previous(jp);
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    // End of variables declaration//GEN-END:variables
}
