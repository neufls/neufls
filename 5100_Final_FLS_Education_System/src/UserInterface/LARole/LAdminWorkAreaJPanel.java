/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.LARole;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author qiufeng
 */
public class LAdminWorkAreaJPanel extends javax.swing.JPanel {
 
    JPanel jp;
    Enterprise enterprise;
    EcoSystem system;
    Network network;
    UserAccount account;
    Organization organization;
    /**
     * Creates new form LAdminWorkAreaJPanel
     */
    public LAdminWorkAreaJPanel(JPanel jp, UserAccount account, Organization organization, Enterprise enterprise, Network network,EcoSystem system) {
        initComponents();
        this.jp=jp;
        this.account=account;
        this.enterprise = enterprise;
        this.system=system;
        
        
        this.network=network;
        this.organization = organization;
        pop();
    }
    
    
    public void pop()
    {
        valueLabel.setText(enterprise.getName());
    }
    
 
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        userJButton = new javax.swing.JButton();
        manageEmployeeJButton = new javax.swing.JButton();
        manageOrganizationJButton = new javax.swing.JButton();
        enterpriseLabel = new javax.swing.JLabel();
        valueLabel = new javax.swing.JLabel();
        Statisticsbtn = new javax.swing.JButton();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Jazz LET", 1, 24)); // NOI18N
        jLabel1.setText("Localization Admin Work-Area");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 40, -1, -1));

        userJButton.setText("Manage User");
        userJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                userJButtonActionPerformed(evt);
            }
        });
        add(userJButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 170, 180, -1));

        manageEmployeeJButton.setText("Manage Employee");
        manageEmployeeJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                manageEmployeeJButtonActionPerformed(evt);
            }
        });
        add(manageEmployeeJButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 130, 180, -1));

        manageOrganizationJButton.setText("Manage Organization");
        manageOrganizationJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                manageOrganizationJButtonActionPerformed(evt);
            }
        });
        add(manageOrganizationJButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 130, 180, -1));

        enterpriseLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        enterpriseLabel.setText("EnterPrise :");
        add(enterpriseLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 90, 120, 30));

        valueLabel.setText("<value>");
        add(valueLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 100, 130, -1));

        Statisticsbtn.setText("Statistics of Subject");
        Statisticsbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                StatisticsbtnActionPerformed(evt);
            }
        });
        add(Statisticsbtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 170, 180, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void userJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_userJButtonActionPerformed
        // TODO add your handling code here:
        ManageUserAccountJPanel muajp = new ManageUserAccountJPanel(jp, enterprise);
        jp.add("ManageUserAccountJPanel", muajp);

        CardLayout layout = (CardLayout) jp.getLayout();
        layout.next(jp);
    }//GEN-LAST:event_userJButtonActionPerformed

    private void manageEmployeeJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_manageEmployeeJButtonActionPerformed

        ManageEmployeeJPanel manageEmployeeJPanel = new ManageEmployeeJPanel(jp, enterprise.getOrganizationDirectory(), enterprise);
        jp.add("manageEmployeeJPanel", manageEmployeeJPanel);

        CardLayout layout = (CardLayout) jp.getLayout();
        layout.next(jp);

    }//GEN-LAST:event_manageEmployeeJButtonActionPerformed

    private void manageOrganizationJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_manageOrganizationJButtonActionPerformed

        ManageOrganizationJPanel manageOrganizationJPanel = new ManageOrganizationJPanel(jp, enterprise.getOrganizationDirectory());
        jp.add("manageOrganizationJPanel", manageOrganizationJPanel);
        CardLayout layout = (CardLayout) jp.getLayout();
        layout.next(jp);
    }//GEN-LAST:event_manageOrganizationJButtonActionPerformed

    private void StatisticsbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_StatisticsbtnActionPerformed
        // TODO add your handling code here:
                ImageJPanel tljp=new ImageJPanel(jp,enterprise,system);
        jp.add("ImageJPanel",tljp);
        CardLayout layout = (CardLayout) jp.getLayout();
        layout.next(jp);
    }//GEN-LAST:event_StatisticsbtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Statisticsbtn;
    private javax.swing.JLabel enterpriseLabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JButton manageEmployeeJButton;
    private javax.swing.JButton manageOrganizationJButton;
    private javax.swing.JButton userJButton;
    private javax.swing.JLabel valueLabel;
    // End of variables declaration//GEN-END:variables
}
