/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.Teacher;

import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.Organization;
import Business.Organization.TeacherOrganization;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author qiufeng
 */
public class MissionDealJPanel extends javax.swing.JPanel {

    /**
     * Creates new form MissionDealJPanel
     */
    UserAccount Teacheraccount;
    Enterprise enterprise;
    Organization organization;
    Network network;
    JPanel jp;
    public MissionDealJPanel(JPanel jp, UserAccount account, Enterprise enterprise, TeacherOrganization organization,Network network) {
        initComponents();
        this.jp = jp;      
       this.enterprise = enterprise;
       this.Teacheraccount = account;
       this.network = network;
        populateMissionTable();
         displaypicUpLabel2();
    }
    
    
  public void displaypicUpLabel2() {
        ImageIcon icon1 = new ImageIcon("pic/5.png");
        icon1.setImage(icon1.getImage().getScaledInstance(60, 50, Image.SCALE_DEFAULT));
        j1.setIcon(icon1);
        
        ImageIcon icon2 = new ImageIcon("pic/18.png");
        icon2.setImage(icon2.getImage().getScaledInstance(100, 100,  Image.SCALE_DEFAULT));
        j2.setIcon(icon2);
  }
    
        public void populateMissionTable(){
        DefaultTableModel dtm = (DefaultTableModel)Missiontbl.getModel();
        dtm.setRowCount(0);

        for(Enterprise e: network.getEnterpriseDirectory().getEnterpriseList()){
            
                 for(WorkRequest wr: e.getWorkQueue().getWorkRequestList()){
                       if(!wr.getClass().getSimpleName().equals("LocalRequest")  ){
                             if(wr.getTeacher().getUsername().equalsIgnoreCase(Teacheraccount.getUsername())){
                            Object[] row = new Object[9];
                                            row[0] = wr;
                                            row[1] = wr.getOnlineTA().getUsername();
                                            row[2] = wr.getRequestDate();
                                            row[3]= wr.getSuject();
                                            row[4] = wr.getWordCount();
                                            row[5] = wr.getStatus();
                                            
                            dtm.addRow(row);
                            
                        }
                    }}}}
                
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Reseacherbtn = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        Missiontbl = new javax.swing.JTable();
        Backbtn = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        j2 = new javax.swing.JLabel();
        j1 = new javax.swing.JLabel();

        Reseacherbtn.setText("Reaserch");
        Reseacherbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ReseacherbtnActionPerformed(evt);
            }
        });

        Missiontbl.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Mission ID", "O-TA", "ApplyDate", "Subject", "Count", "Status"
            }
        ));
        jScrollPane2.setViewportView(Missiontbl);

        Backbtn.setText("Back");
        Backbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BackbtnActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Lucida Sans Typewriter", 1, 24)); // NOI18N
        jLabel1.setText("Research Area");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(66, 66, 66)
                        .addComponent(Backbtn)
                        .addGap(133, 133, 133)
                        .addComponent(Reseacherbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(j2, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 620, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(39, 39, 39)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(j1, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(57, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(46, 46, 46)
                        .addComponent(jLabel1)
                        .addGap(31, 31, 31))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(j1, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Backbtn)
                    .addComponent(Reseacherbtn)
                    .addComponent(j2, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(277, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void ReseacherbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ReseacherbtnActionPerformed
        // TODO add your handling code here:
          int selectedRow = Missiontbl.getSelectedRow();
        
        
           if(selectedRow >= 0){
        
            for(Enterprise e: network.getEnterpriseDirectory().getEnterpriseList()){
            
         
               for(WorkRequest wr: e.getWorkQueue().getWorkRequestList()){
                   
               
                   if(!wr.getClass().getSimpleName().equals("LocalRequest")  ){
                      
                        if(wr==Missiontbl.getValueAt(selectedRow, 0)){
                    
                            if(wr.getStatus().equals(WorkRequest.workStatus.waitingRes.name())){
                        
                     
                        wr.setStatus(WorkRequest.workStatus.Researched.name());
                       
                        populateMissionTable();
                            }
                            else{
                        JOptionPane.showMessageDialog(null,"Already Reaserch");
                            }
                        }
                 
                    }
                }
             
                
                }
                                
        }else{
            
            JOptionPane.showMessageDialog(null,"Please select any Mission");
        }
    }//GEN-LAST:event_ReseacherbtnActionPerformed

    private void BackbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BackbtnActionPerformed
        // TODO add your handling code here:
        jp.remove(this);
        CardLayout layout = (CardLayout) jp.getLayout();
        layout.previous(jp);
    }//GEN-LAST:event_BackbtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Backbtn;
    private javax.swing.JTable Missiontbl;
    private javax.swing.JButton Reseacherbtn;
    private javax.swing.JLabel j1;
    private javax.swing.JLabel j2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane2;
    // End of variables declaration//GEN-END:variables
}
