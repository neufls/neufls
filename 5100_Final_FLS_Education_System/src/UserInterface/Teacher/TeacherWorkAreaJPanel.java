/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.Teacher;

import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.Organization;
import Business.Organization.TeacherOrganization;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.LocalRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author qiufeng
 */
public class TeacherWorkAreaJPanel extends javax.swing.JPanel {

    /**
     * Creates new form TeacherWorkAreaJPanel
     */
    UserAccount Teacheraccount;
    Enterprise enterprise;
    Organization organization;
    Network network;
    JPanel jp;
    private String path;

    public TeacherWorkAreaJPanel(JPanel jp, UserAccount account, Enterprise enterprise, TeacherOrganization organization, Network network) {
        initComponents();
        this.jp = jp;

        this.enterprise = enterprise;
        this.Teacheraccount = account;
        this.network = network;
        this.path = account.getPath();

        populateTable();
        //  populateMissionTable();
displaypicUpLabel2() ;
    }

                public void displaypicUpLabel2() {
        ImageIcon icon1 = new ImageIcon("pic/20.png");
        icon1.setImage(icon1.getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT));
        j1.setIcon(icon1);
//        
//        ImageIcon icon2 = new ImageIcon("pic/7.png");
//        icon2.setImage(icon2.getImage().getScaledInstance(70, 50,  Image.SCALE_DEFAULT));
//        j2.setIcon(icon2);
                }
                
    public void populateTable() {

        DefaultTableModel dtm = (DefaultTableModel) Teachertbl.getModel();
        dtm.setRowCount(0);

        //  System.out.println("____1______"+network.getEnterpriseDirectory().getEnterpriseList());
        for (Enterprise e : network.getEnterpriseDirectory().getEnterpriseList()) {

            //   System.out.println("____2______"+e.getClass().getSimpleName());
            if (e.getClass().getSimpleName().equals("Localenterprise")) {

                //       System.out.println("____3______"+e.getWorkQueue().getWorkRequestList());
                for (WorkRequest wr : e.getWorkQueue().getWorkRequestList()) {

                    if (wr.getClass().getSimpleName().equals("LocalRequest")) {

                        LocalRequest lr = (LocalRequest) wr;
//                                                
                        System.out.println("____3______" + wr.getStudentName());
                        System.out.println("____4______" + lr.getStudentName());
//                      System.out.println("____5______"+lr.getTeacher().getUsername());
                        if (lr != null && lr.getTeacher() != null && lr.getTeacher().getUsername().equalsIgnoreCase(Teacheraccount.getUsername())) {
                            Object[] row = new Object[9];
                            row[0] = lr;
                            row[1] = lr.getStudentName();
                            row[2] = e.getName();
                            row[3] = lr.getRequestDate();
                            row[4] = lr.getSuject();
                            row[5] = lr.getLocalTA();
                            row[6] = lr.getOnlineTA();
                            row[7] = lr.getStatus();

                            dtm.addRow(row);

                        }

                    }
                }
            }

        }
//            for(Enterprise e: network.getEnterpriseDirectory().getEnterpriseList()){
//      
//            System.out.println("____2______"+e.getClass().getSimpleName());
//            if(e.getClass().getSimpleName().equals("Localenterprise")){
//       
//                 System.out.println("____3______"+e.getWorkQueue().getWorkRequestList());
//                for(WorkRequest wr: e.getWorkQueue().getWorkRequestList()){
//                      
//                    if(wr.getClass().getSimpleName().equals("LocalRequest")  ){
//                        LocalRequest lr = (LocalRequest) wr;
//                        
//                         System.out.println("____4______"+lr.getTeacher());
//                         System.out.println("____5______"+Teacheraccount.getUsername());
//                        if(lr.getTeacher().getUsername().equalsIgnoreCase(Teacheraccount.getUsername())){
//                        
//                Object[] row = new Object[10];
//                row[0] = lr;
//                row[1] = lr.getStatus();
//                row[2] = e.getName();
//                row[3] = lr.getSuject();
//                row[4] = lr.getLocalTA();
//                row[5] = lr.getOnlineTA();
//                row[6] = lr.getStatus();
//    }
//               }
//    
//                }}}

    }

    public void populateMissionTable() {
        DefaultTableModel dtm = (DefaultTableModel) Teachertbl.getModel();
        dtm.setRowCount(0);

        for (Enterprise e : network.getEnterpriseDirectory().getEnterpriseList()) {

            if (e.getClass().getSimpleName().equals("Localenterprise")) {

                for (WorkRequest wr : e.getWorkQueue().getWorkRequestList()) {

                    if (!wr.getClass().getSimpleName().equals("LocalRequest")) {

                        if (wr.getTeacher().getUsername().equalsIgnoreCase(Teacheraccount.getUsername())) {
                            Object[] row = new Object[9];
                            row[0] = wr;
                            row[1] = wr.getOnlineTA().getUsername();
                            row[2] = wr.getRequestDate();
                            row[3] = wr.getSuject();
                            row[4] = wr.getWordCount();
                            row[5] = wr.getStatus();

                            dtm.addRow(row);

                        }

                    }
                }
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        Teachertbl = new javax.swing.JTable();
        Teacherbtn = new javax.swing.JButton();
        Backbtn = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        j1 = new javax.swing.JLabel();

        Teachertbl.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Lession ID", "Student", "Local-Enterprise", "ApplyDate", "Subject", "LocalTA", "OnlineTA", "Status"
            }
        ));
        jScrollPane1.setViewportView(Teachertbl);

        Teacherbtn.setText("Teach");
        Teacherbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TeacherbtnActionPerformed(evt);
            }
        });

        Backbtn.setText("Back");
        Backbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BackbtnActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Superclarendon", 1, 32)); // NOI18N
        jLabel1.setText("Student Request List");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 627, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addComponent(Backbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(165, 165, 165)
                        .addComponent(Teacherbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(j1, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(86, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(96, 96, 96)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Backbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Teacherbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addComponent(j1, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(241, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void TeacherbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TeacherbtnActionPerformed
        // TODO add your handling code here:

        int selectedRow = Teachertbl.getSelectedRow();

        if (selectedRow >= 0) {

            for (Enterprise e : network.getEnterpriseDirectory().getEnterpriseList()) {

                //   System.out.println("*****111**********"+e.getClass().getSimpleName());
                if (e.getClass().getSimpleName().equals("Localenterprise")) {
                    //    
                    //   System.out.println("*****2222**********"+e.getWorkQueue().getWorkRequestList());
                    for (WorkRequest wr : e.getWorkQueue().getWorkRequestList()) {

                        //      System.out.println("*****3333**********"+wr.getClass().getSimpleName());
                        if (wr.getClass().getSimpleName().equals("LocalRequest")) {
                            LocalRequest lr = (LocalRequest) wr;

                            //     System.out.println("*****444**********"+lr);
                            ///       System.out.println("*****4455**********"+localbtn.getValueAt(selectedRow, 0));
                            if (lr == Teachertbl.getValueAt(selectedRow, 0)) {

                                //        System.out.println("*****5555555**********"+lr.getStatus());
                                if (lr.getStatus().equals(WorkRequest.workStatus.Allocated.name())) {

                                    LessionDealJPanel panel = new LessionDealJPanel(jp, lr, enterprise);
                                    jp.add("createWorkJPanel", panel);
                                    CardLayout layout = (CardLayout) jp.getLayout();
                                    layout.next(jp);

                                    //    lr.setStatus(WorkRequest.workStatus.Teached.name());                    
                                    //   populateTable();
                                } else {
                                    JOptionPane.showMessageDialog(null, "Already Teacher");
                                }
                            }
                            //     System.out.println("*****666666**********");
                        }
                    }
                }

            }

        } else {

            JOptionPane.showMessageDialog(null, "Please select any Project");
        }

    }//GEN-LAST:event_TeacherbtnActionPerformed

    private void BackbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BackbtnActionPerformed
        // TODO add your handling code here:
        jp.remove(this);
        CardLayout layout = (CardLayout) jp.getLayout();
        layout.previous(jp);
    }//GEN-LAST:event_BackbtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Backbtn;
    private javax.swing.JButton Teacherbtn;
    private javax.swing.JTable Teachertbl;
    private javax.swing.JLabel j1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
