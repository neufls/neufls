/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.OTA;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.LocalRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author qiufeng
 */
public class LocalRequestJPanel extends javax.swing.JPanel {

    /**
     * Creates new form LocalRequestJPanel
     */
    JPanel upc;

    Enterprise enterprise;
    UserAccount OnlinTAAccount;
    Network network;

    public LocalRequestJPanel(JPanel upc, Enterprise enterprise, UserAccount account, Network network) {
        initComponents();
        this.upc = upc;

        this.enterprise = enterprise;
        this.OnlinTAAccount = account;
        this.network = network;

        // System.out.printf("111111111"+ network.getEnterpriseDirectory().getEnterpriseList());
        populateCombo();
        populateTable();
        displaypicUpLabel2();
        populateTeacher(JCSubject.getSelectedItem().toString());

    }

          public void displaypicUpLabel2() {
        ImageIcon icon1 = new ImageIcon("pic/9.png");
        icon1.setImage(icon1.getImage().getScaledInstance(60, 50, Image.SCALE_DEFAULT));
        j1.setIcon(icon1);
        
        ImageIcon icon2 = new ImageIcon("pic/6.png");
        icon2.setImage(icon2.getImage().getScaledInstance(40, 40,  Image.SCALE_DEFAULT));
        j4.setIcon(icon2);
        
        ImageIcon icon3 = new ImageIcon("pic/16.png");
        icon3.setImage(icon3.getImage().getScaledInstance(100,100, Image.SCALE_DEFAULT));
        j3.setIcon(icon3);
        
//        ImageIcon icon4 = new ImageIcon("pic/6.png");
//        icon4.setImage(icon4.getImage().getScaledInstance(40, 40,  Image.SCALE_DEFAULT));
//        j4.setIcon(icon4);
    }
          
    public void populateTeacher(String Sbuject) {

        DefaultTableModel model = (DefaultTableModel) Teacherbtl.getModel();

        model.setRowCount(0);

        //  System.out.println("11111"+enterprise.getUserAccountDirectory().getUserAccountList());
        for (UserAccount ua : enterprise.getUserAccountDirectory().getUserAccountList()) {

            if (ua.getRole().toString().equals("TeacherRole")) {
                //     System.out.println("22222"+ua.getUsername());
                if (ua.getSubject().equalsIgnoreCase(Sbuject)) {
                    //     System.out.println("3333333"+ua.getUsername());

                    Object[] row = new Object[2];
                    row[0] = ua.getUsername();
                    model.addRow(row);

                }
            }
        }

    }

    public void populateCombo() {

//        cbPM.removeAllItems();
//        for(UserAccount ua: enterprise.getUserAccountDirectory().getUserAccountList()){
//            if(ua.getRole().toString().equals("TeacherRole")){
//                cbPM.addItem(ua.getEmployee().getName());
//            }
//        }
//    
    }

    public void populateTable() {

        DefaultTableModel dtm = (DefaultTableModel) localbtn.getModel();
        dtm.setRowCount(0);

        //System.out.println("***************");
        for (Enterprise e : network.getEnterpriseDirectory().getEnterpriseList()) {

            // System.out.println("23232323"+e.getClass().getSimpleName());
            if (e.getClass().getSimpleName().equals("Localenterprise")) {

                //    System.out.println(e.getName());
                for (WorkRequest wr : e.getWorkQueue().getWorkRequestList()) {

                    //      System.out.println("*****123**********"+ wr.getClass().getSimpleName());
                    if (wr.getClass().getSimpleName().equals("LocalRequest")) {
                        // System.out.println("*******************************");
                        //System.out.println("111111"+wr.getEmali());
                        LocalRequest lr = (LocalRequest) wr;
                       // System.out.println("22222222"+lr.getOnlineEnterprise());
                       // System.out.println("lr enter name "+lr.getOnlineEnterprise());
                       // System.out.println("enter name"+enterprise.getName());
                       // System.out.println("*******************************");
                        if (lr != null && lr.getOnlineEnterprise() != null && lr.getOnlineEnterprise().equals(enterprise.getName())) {
                            Object[] row = new Object[8];
                            row[0] = lr.getLessionID();
                            row[1] = lr.getStudentName();
                            row[2] = e.getName();
                            row[3] = lr.getSuject();
                            row[4] = lr.getRequestDate();
                            row[5] = lr.getLocalizeUnitPrice();
                            row[6] = lr.getStatus();
                            dtm.addRow(row);

                        }

                    }
                }
            }

        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        localbtn = new javax.swing.JTable();
        Allocatedbtn = new javax.swing.JButton();
        Backbtn = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        Teacherbtl = new javax.swing.JTable();
        JCSubject = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        j1 = new javax.swing.JLabel();
        j4 = new javax.swing.JLabel();
        j3 = new javax.swing.JLabel();

        localbtn.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Lession ID", "Student", "Local-Enterprise", "Subject", "ApplyDate", "Price", "Status"
            }
        ));
        jScrollPane1.setViewportView(localbtn);

        Allocatedbtn.setText("Allocated to Teacher");
        Allocatedbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AllocatedbtnActionPerformed(evt);
            }
        });

        Backbtn.setText("Back");
        Backbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BackbtnActionPerformed(evt);
            }
        });

        jLabel1.setText("Allocated to Teacher:");

        Teacherbtl.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null},
                {null},
                {null},
                {null}
            },
            new String [] {
                "Teacher"
            }
        ));
        jScrollPane2.setViewportView(Teacherbtl);

        JCSubject.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Math", "English", "Science", "Physics", "Chineses", " " }));
        JCSubject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JCSubjectActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Wawati TC", 1, 36)); // NOI18N
        jLabel2.setText("Local Request Work Area");

        jLabel6.setFont(new java.awt.Font("Xingkai SC", 3, 13)); // NOI18N
        jLabel6.setText("Step2");

        jLabel4.setFont(new java.awt.Font("Xingkai SC", 3, 13)); // NOI18N
        jLabel4.setText("Step1\n");

        jLabel7.setFont(new java.awt.Font("Xingkai SC", 3, 13)); // NOI18N
        jLabel7.setText("Step4");

        jLabel8.setFont(new java.awt.Font("Xingkai SC", 3, 24)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 51, 51));
        jLabel8.setText("Step3:Allocate to teacher ");

        jLabel3.setFont(new java.awt.Font("Microsoft Sans Serif", 1, 14)); // NOI18N
        jLabel3.setText("Tip: Please select the teacher of the corresponding subject.");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Backbtn)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(j1, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(5, 5, 5)
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel8)))
                        .addGap(659, 659, 659)
                        .addComponent(jLabel7)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(j4, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(Allocatedbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 325, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(6, 6, 6)
                                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(67, 67, 67)
                                        .addComponent(j3, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(JCSubject, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 647, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(jLabel6)
                        .addComponent(jLabel7)
                        .addComponent(jLabel8))
                    .addComponent(j1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(100, 100, 100)
                        .addComponent(Backbtn))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(48, 48, 48)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel1)
                                    .addComponent(JCSubject, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(j4, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(Allocatedbtn))
                            .addComponent(j3, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(74, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void BackbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BackbtnActionPerformed
        // TODO add your handling code here:
        upc.remove(this);
        CardLayout layout = (CardLayout) upc.getLayout();
        layout.previous(upc);

    }//GEN-LAST:event_BackbtnActionPerformed

    private void AllocatedbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AllocatedbtnActionPerformed
        // TODO add your handling code here:

        int selectedRow = localbtn.getSelectedRow();
        int selectTea = Teacherbtl.getSelectedRow();

        //System.out.println("======1" + selectTea);

        //   System.out.println("======2" + TeachetName);
//        String TeachetName = cbPM.getSelectedItem().toString();
        if (selectedRow >= 0 && selectTea >= 0) {

            for (Enterprise e : network.getEnterpriseDirectory().getEnterpriseList()) {

//             System.out.println("*****111**********"+e.getClass().getSimpleName());
                if (e.getClass().getSimpleName().equals("Localenterprise")) {

                    //
                    for (WorkRequest wr : e.getWorkQueue().getWorkRequestList()) {

                        //    System.out.println("*****3333**********"+wr.getClass().getSimpleName());
                        if (wr.getClass().getSimpleName().equals("LocalRequest")) {
                            LocalRequest lr = (LocalRequest) wr;

//                         System.out.println("*****444**********"+lr);
//                         System.out.println("*****4455**********"+localbtn.getValueAt(selectedRow, 0));
//                         
                            if (lr.getLessionID() == (int) localbtn.getValueAt(selectedRow, 0)) {

                                //        System.out.println("*****5555555**********"+lr.getStatus());
                                if (lr.getStatus().equals(WorkRequest.workStatus.LocalizeRequested.name())) {
                                        System.out.println("======*********" + lr.getSuject());
                                        System.out.println("======%%%%%%%%%" + JCSubject.getSelectedItem().toString());

                                    if (lr.getSuject().equals(JCSubject.getSelectedItem().toString())) {

                                        String TeachetName = (String) Teacherbtl.getValueAt(selectTea, 0);
                                        UserAccount Teacher = enterprise.getUserAccountDirectory().findUserAccountBYname2(TeachetName);
                                        //System.out.println("======3" + Teacher.getUsername());

                                        lr.setOnlineTA(OnlinTAAccount);
                                        lr.setStatus(WorkRequest.workStatus.Allocated.name());
                                        lr.setTeacher(Teacher);

                                        //System.out.println("======4" + lr.getTeacher().getUsername());
                                        // System.out.println(Teacher.getUsername());

                                        //     System.out.println(lr.getTeacher());
                                        populateTable();
                                        JOptionPane.showMessageDialog(null, "The Project is successfully allocated");

                                    } else {
                                        JOptionPane.showMessageDialog(null, "You should select a teacher who teaches the selected course");

                                    }

                                } else {
                                    JOptionPane.showMessageDialog(null, "Already Allocated to Teacher");
                                }
                            }
                            //  System.out.println("*****666666**********");
                        }
                    }
                }

            }

        } else {

            JOptionPane.showMessageDialog(null, "Please select two rows");
        }


    }//GEN-LAST:event_AllocatedbtnActionPerformed

    private void JCSubjectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JCSubjectActionPerformed
        // TODO add your handling code here:
        String subject = JCSubject.getSelectedItem().toString();
        if (subject != null) {
            populateTeacher(subject);
        }
    }//GEN-LAST:event_JCSubjectActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Allocatedbtn;
    private javax.swing.JButton Backbtn;
    private javax.swing.JComboBox JCSubject;
    private javax.swing.JTable Teacherbtl;
    private javax.swing.JLabel j1;
    private javax.swing.JLabel j3;
    private javax.swing.JLabel j4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable localbtn;
    // End of variables declaration//GEN-END:variables
}
