/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.LStudent;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Organization.LocalstudentOrganization;
import Business.Organization.Organization;
import Business.RateInfo.RateInfo;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.LocalRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.awt.Image;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author qiufeng
 */
public class StudentWorkAreaJPanel extends javax.swing.JPanel {

    /**
     * Creates new form StudentWorkAreaJPanel
     */
    LocalstudentOrganization organization;
    JPanel upc;
    EcoSystem system;
    Enterprise enterprise;
    UserAccount LocalStudent;
    private String path;
//    private String p1="pic/8.png";
//    private String p2="pic/16.png";

    public StudentWorkAreaJPanel(JPanel jp, UserAccount account, Enterprise enterprise, LocalstudentOrganization organization) {
        initComponents();
        this.upc = jp;
        this.organization = organization;

        this.enterprise = enterprise;
        this.LocalStudent = account;

        this.path = account.getPath().toString();
//        System.out.println("1111111" + account.getPath());
//        System.out.println("2222222" + path);

        jLabel2.setText(account.getUsername());
        jLabel4.setText(enterprise.getName());
        //System.out.printf("4444444444444");
        //System.out.println(enterprise.getEnterpriseType() + " " + enterprise.getName());
        // System.out.println(enterprise.getWorkQueue().getWorkRequestList());
        populateTable();
        displaypicUpLabel();
        displaypicUpLabel2();

    }

    public void displaypicUpLabel2() {
        ImageIcon icon1 = new ImageIcon("pic/9.png");
        icon1.setImage(icon1.getImage().getScaledInstance(60, 50, Image.SCALE_DEFAULT));
        j1.setIcon(icon1);

        ImageIcon icon2 = new ImageIcon("pic/16.png");
        icon2.setImage(icon2.getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT));
        j2.setIcon(icon2);

        ImageIcon icon3 = new ImageIcon("pic/11.png");
        icon3.setImage(icon3.getImage().getScaledInstance(45, 45, Image.SCALE_DEFAULT));
        j3.setIcon(icon3);

        ImageIcon icon4 = new ImageIcon("pic/6.png");
        icon4.setImage(icon4.getImage().getScaledInstance(40, 40, Image.SCALE_DEFAULT));
        j4.setIcon(icon4);
    }

    public void populateTable() {

        DefaultTableModel dtm = (DefaultTableModel) studenttbl.getModel();
        dtm.setRowCount(0);
        //   System.out.printf("111111");
        // System.out.println("Workrequest"+enterprise.getWorkQueue().getWorkRequestList());
        for (WorkRequest wr : enterprise.getWorkQueue().getWorkRequestList()) {
            LocalRequest lr = (LocalRequest) wr;
            if (lr.getStudentName().equalsIgnoreCase(LocalStudent.getUsername())) {
                Object[] row = new Object[7];
                row[0] = lr;
                row[1] = lr.getSuject();
                row[2] = lr.getStatus();
                row[3] = lr.getLessionAddress();
                dtm.addRow(row);
            }
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        studentsbl = new javax.swing.JScrollPane();
        studenttbl = new javax.swing.JTable();
        picUpLabel = new javax.swing.JLabel();
        Addbtn = new javax.swing.JButton();
        scoreField = new javax.swing.JTextField();
        submitBtn = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        j1 = new javax.swing.JLabel();
        j2 = new javax.swing.JLabel();
        j3 = new javax.swing.JLabel();
        j4 = new javax.swing.JLabel();
        SliderScore = new javax.swing.JSlider();

        jLabel1.setFont(new java.awt.Font("Krungthep", 1, 24)); // NOI18N
        jLabel1.setText("Student:");

        jLabel2.setFont(new java.awt.Font("Lucida Grande", 0, 24)); // NOI18N
        jLabel2.setText("jLabel2");

        jLabel3.setFont(new java.awt.Font("Lucida Sans Typewriter", 1, 24)); // NOI18N
        jLabel3.setText("From:");

        jLabel4.setFont(new java.awt.Font("Lucida Grande", 0, 24)); // NOI18N
        jLabel4.setText("jLabel4");

        studenttbl.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Lession ID", "Subject", "Status"
            }
        ));
        studentsbl.setViewportView(studenttbl);
        if (studenttbl.getColumnModel().getColumnCount() > 0) {
            studenttbl.getColumnModel().getColumn(2).setResizable(false);
        }

        Addbtn.setText("Apply for a new lesson");
        Addbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AddbtnActionPerformed(evt);
            }
        });

        scoreField.setEditable(false);
        scoreField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                scoreFieldActionPerformed(evt);
            }
        });

        submitBtn.setText("Score");
        submitBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitBtnActionPerformed(evt);
            }
        });

        jLabel5.setText("☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆☆");

        jLabel6.setFont(new java.awt.Font("Wawati TC", 3, 24)); // NOI18N
        jLabel6.setText("Lesson List:");

        jLabel7.setText("Score Here:");

        jLabel8.setText("Pease select a lesson which status is \"Teached\" and");

        jLabel9.setText("give a score for the lesson.");

        jLabel10.setFont(new java.awt.Font("Xingkai TC", 3, 24)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 51, 51));
        jLabel10.setText("Step1:Apply ");

        jLabel11.setFont(new java.awt.Font("Xingkai TC", 1, 13)); // NOI18N
        jLabel11.setText("Step2");

        jLabel12.setFont(new java.awt.Font("Xingkai TC", 1, 13)); // NOI18N
        jLabel12.setText("Step3");

        jLabel13.setFont(new java.awt.Font("Xingkai TC", 1, 13)); // NOI18N
        jLabel13.setText("Step4");

        SliderScore.setMajorTickSpacing(10);
        SliderScore.setMinimum(10);
        SliderScore.setValue(60);
        SliderScore.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                SliderScoreStateChanged(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(Addbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(j2, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(139, 139, 139)
                                .addComponent(j1, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel6)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel10)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel11)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel12)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel13))
                                    .addComponent(studentsbl, javax.swing.GroupLayout.PREFERRED_SIZE, 502, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(18, 18, 18)
                        .addComponent(picUpLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(274, 274, 274))
            .addGroup(layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(j4, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(SliderScore, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(scoreField, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(submitBtn))
                            .addComponent(jLabel9))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(j3, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel2))
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(16, 16, 16)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel10)
                                .addComponent(jLabel11)
                                .addComponent(jLabel12)
                                .addComponent(jLabel13))
                            .addComponent(j1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jLabel6))
                    .addComponent(picUpLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(studentsbl, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Addbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(j2, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel8))
                    .addComponent(j4, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(submitBtn)
                            .addComponent(scoreField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(j3, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(5, 5, 5)
                .addComponent(SliderScore, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(250, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void AddbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AddbtnActionPerformed
        // TODO add your handling code here:
        StudentAddJPanel panel = new StudentAddJPanel(upc, enterprise, LocalStudent);
        upc.add("createWorkJPanel", panel);
        CardLayout layout = (CardLayout) upc.getLayout();
        layout.next(upc);

    }//GEN-LAST:event_AddbtnActionPerformed

    private void scoreFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_scoreFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_scoreFieldActionPerformed

    private void submitBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submitBtnActionPerformed
        // TODO add your handling code here:
        
       // String a = scoreField.getText();

        if (scoreField.getText() != null && !scoreField.getText().equals("")) {

//            int num;
//            boolean isNum = false;
//
//            lableC:
//
//            try {
//                num = Integer.parseInt(a);
//                isNum = true;
//            } catch (Exception e) {
//                JOptionPane.showMessageDialog(null, "Please input an integer");
//                break lableC;
//            }

            int selectedProject = studenttbl.getSelectedRow();
            int score = Integer.parseInt(scoreField.getText());

            int baseScore = 0;
            //lableB:

            if (score > 100 || score < 0) {
                JOptionPane.showMessageDialog(null, "Please input score between 0~100");
                //break lableB;
            } else {
                if (selectedProject >= 0) {
                    LocalRequest lr = (LocalRequest) studenttbl.getValueAt(selectedProject, 0);
                    if (lr != null && lr.getStatus() != null && lr.getStatus().equals(WorkRequest.workStatus.Teached.name())) {
                        //System.out.println(lr.getOnlineEnterprise() + "333333333");
                        RateInfo si = enterprise.getRateDirectory().findInfoByName(lr.getOnlineEnterprise());
                        //System.out.println("qwertyui" + enterprise.getRateDirectory());
                        //System.out.println(si + "&&&&&&");
                        int count = si.getCount();
                        baseScore = si.getRate();
                        baseScore =((baseScore*count+score)/(count+1));
                        si.setRate(baseScore);
                        si.setCount(count+1);
                        //System.out.println(baseScore);
                        lr.setStatus(WorkRequest.workStatus.Scored.name());
                        populateTable();
                        JOptionPane.showMessageDialog(null, "You have successfully scored");

                    } else {
                        JOptionPane.showMessageDialog(null, "You cannot grade now");
                    }

                } else {
                    JOptionPane.showMessageDialog(null, "Please select any Row");
                }

            }

        } else {
            JOptionPane.showMessageDialog(null, "Please input your score");
        }
    }//GEN-LAST:event_submitBtnActionPerformed

    private void SliderScoreStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_SliderScoreStateChanged
        // TODO add your handling code here:
        int score = SliderScore.getValue();
        //SliderScore.setValue(score);
        scoreField.setText(String.valueOf(score));
    }//GEN-LAST:event_SliderScoreStateChanged

    public void displaypicUpLabel() {
        ImageIcon icon = new ImageIcon(path);
        System.out.print(this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath());
        icon.setImage(icon.getImage().getScaledInstance(145, 155, Image.SCALE_DEFAULT));
        picUpLabel.setIcon(icon);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Addbtn;
    private javax.swing.JSlider SliderScore;
    private javax.swing.JLabel j1;
    private javax.swing.JLabel j2;
    private javax.swing.JLabel j3;
    private javax.swing.JLabel j4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel picUpLabel;
    private javax.swing.JTextField scoreField;
    private javax.swing.JScrollPane studentsbl;
    private javax.swing.JTable studenttbl;
    private javax.swing.JButton submitBtn;
    // End of variables declaration//GEN-END:variables
}
