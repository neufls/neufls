/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import static Business.Organization.Organization.Type.Teacher;
import Business.UserAccount.UserAccount;
import java.util.Date;
import java.util.Random;

/**
 *
 * @author qiufeng
 */
public class WorkRequest {
    
    
   
    private static int count = 30000;
    private int lessionID;
    private String StudentName;
    private String Suject;
    private double price;
    
    private UserAccount Teacher;
    private String status;
    private Date requestDate;
    private Date resolveDate;
    private String content;
    private int wordCount;
    private String mission;
    private String description;
    private String emali;
    private String teacheranno;
    
         private UserAccount OnlineTA;
     
    
     public WorkRequest(){
        requestDate = new Date();
        status = workStatus.Applying.name();
        lessionID = randInt(20000,40000);
        
//        this.teacher =  new UserAccount();
    }
 
     
    public enum Suject{
        Math,English,Science,Physics,Chineses;
    }
        
    public enum workStatus{
        Applying,LocalizeReceived,LocalizeRequested,Scored,
       Allocated,Teaching,Teached,Researching,Researched,waitingRes;

    }
    
    
    
    
    
    public static int randInt(int min, int max) {

    // NOTE: Usually this should be a field rather than a method
    // variable so that it is not re-seeded every call.
    Random rand = new Random();

    // nextInt is normally exclusive of the top value,
    // so add 1 to make it inclusive
    int randomNum = rand.nextInt((max - min) + 1) + min;

    return randomNum;
}

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getWordCount() {
        return wordCount;
    }

    public void setWordCount(int wordCount) {
        this.wordCount = wordCount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmali() {
        return emali;
    }

    public void setEmali(String emali) {
        this.emali = emali;
    }

    public String getTeacheranno() {
        return teacheranno;
    }

    public void setTeacheranno(String teacheranno) {
        this.teacheranno = teacheranno;
    }

    
    
    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        WorkRequest.count = count;
    }

    public int getLessionID() {
        return lessionID;
    }

    public void setLessionID(int lessionID) {
        this.lessionID = lessionID;
    }

    public String getStudentName() {
        return StudentName;
    }

    public void setStudentName(String StudentName) {
        this.StudentName = StudentName;
    }

    public String getSuject() {
        return Suject;
    }

    public void setSuject(String Suject) {
        this.Suject = Suject;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public UserAccount getOnlineTA() {
        return OnlineTA;
    }

    public void setOnlineTA(UserAccount OnlineTA) {
        this.OnlineTA = OnlineTA;
    }



    public UserAccount getTeacher() {
        return Teacher;
    }

    public void setTeacher(UserAccount Teacher) {
        this.Teacher = Teacher;
    }



    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getResolveDate() {
        return resolveDate;
    }

    public void setResolveDate(Date resolveDate) {
        this.resolveDate = resolveDate;
    }
     public String toString(){
        return String.valueOf(lessionID);
    }   
}
