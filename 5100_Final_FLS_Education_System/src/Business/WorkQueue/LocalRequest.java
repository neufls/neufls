/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.Enterprise.Localenterprise;
import Business.UserAccount.UserAccount;

/**
 *
 * @author qiufeng
 */
public class LocalRequest extends WorkRequest {
        
    public LocalRequest(){
        super();
        this.setStatus(workStatus.LocalizeReceived.name());
    }
    
    Localenterprise localizationCompany;
    
    String OnlineEnterprise; //储存网上的公司
    
    private double LocalizeUnitPrice;
     private UserAccount LocalStudent;
     private UserAccount LocalTA;
     private String LessionAddress;

     private double profit;

    public String getLessionAddress() {
        return LessionAddress;
    }

    public void setLessionAddress(String LessionAddress) {
        this.LessionAddress = LessionAddress;
    }

     
     
     
     
    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }
     
     
     
     
     
    public double getLocalizeUnitPrice() {
        return LocalizeUnitPrice;
    }

    public void setLocalizeUnitPrice(double LocalizeUnitPrice) {
        this.LocalizeUnitPrice = LocalizeUnitPrice;
    }


     
  
    public Localenterprise getLocalizationCompany() {
        return localizationCompany;
    }

        public UserAccount getLocalTA() {
        return LocalTA;
    }

    public void setLocalTA(UserAccount LocalTA) {
        this.LocalTA = LocalTA;
    }


    public void setLocalizationCompany(Localenterprise localizationCompany) {
        this.localizationCompany = localizationCompany;
    }

    public String getOnlineEnterprise() {
        return OnlineEnterprise;
    }

    public void setOnlineEnterprise(String OnlineEnterprise) {
        this.OnlineEnterprise = OnlineEnterprise;
    }

    public UserAccount getLocalStudent() {
        return LocalStudent;
    }

    public void setLocalStudent(UserAccount LocalStudent) {
        this.LocalStudent = LocalStudent;
    }


    public boolean equals(LocalRequest obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return obj.getLessionID() == this.getLessionID();
    }

    
    
    
    
    
    
    
    
}
