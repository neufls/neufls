/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Enterprise.Localenterprise;
import Business.Network.Network;
import Business.Organization.LocalTAOrganization;
import Business.Organization.Organization;

import Business.UserAccount.UserAccount;
import UserInterface.LARole.LAdminWorkAreaJPanel;
import UserInterface.OARole.OAdminWorkAreaJPanel;


import javax.swing.JPanel;
//import userInterface.LRole.LAdminWorkAreaJPanel;
//import userInterface.TRole.TAdminWorkAreaJPanel;

/**
 *
 * @author qiufeng
 */
public class AdminRole extends Role{
    


    @Override
    public JPanel createWorkArea( JPanel jp, UserAccount account, Organization organization, Enterprise enterprise, Network network,EcoSystem system) {
        
        if(enterprise.getEnterpriseType().getValue().equals("Localization Company"))
        {
            return new LAdminWorkAreaJPanel( jp,  account, organization, enterprise, network, system);
        }
        if(enterprise.getEnterpriseType().getValue().equals("Online Company"))
        {
            return new OAdminWorkAreaJPanel( jp, account, organization, enterprise, network, system);
        }

        return null;

    }  
}
