/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Enterprise.Localenterprise;
import Business.Network.Network;
import Business.Organization.LocalTAOrganization;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import UserInterface.LTA.LocalTAWorkAreaJPanel;
import javax.swing.JPanel;

/**
 *
 * @author qiufeng
 */
public class LocalTARole extends Role{
    
  
    @Override
    public JPanel createWorkArea( JPanel jp, UserAccount account, Organization organization, Enterprise enterprise, Network network,EcoSystem business) {
        return new LocalTAWorkAreaJPanel(jp, account, (Localenterprise)enterprise, (LocalTAOrganization)organization, network);
    }
    
}
