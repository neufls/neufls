/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Enterprise.Onlineenterprise;
import Business.Network.Network;
import Business.Organization.LocalTAOrganization;
import Business.Organization.OnlineTAOrganization;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import UserInterface.OTA.OnlineTAWorkAreaJPanel;
import javax.swing.JPanel;

/**
 *
 * @author qiufeng
 */
public class OnlineTARole extends Role{
        @Override
    public JPanel createWorkArea( JPanel jp, UserAccount account, Organization organization, Enterprise enterprise, Network network,EcoSystem business) {
        return new OnlineTAWorkAreaJPanel(jp, account, (Onlineenterprise)enterprise, (OnlineTAOrganization)organization, network);
    }
}
