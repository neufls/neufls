/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Enterprise.Onlineenterprise;
import Business.Network.Network;
import Business.Organization.LocalTAOrganization;
import Business.Organization.Organization;
import Business.Organization.TeacherOrganization;
import Business.UserAccount.UserAccount;
import UserInterface.Teacher.TeacherSurface;
import UserInterface.Teacher.TeacherWorkAreaJPanel;
import javax.swing.JPanel;

/**
 *
 * @author qiufeng
 */
public class TeacherRole extends Role{
        @Override
    public JPanel createWorkArea(JPanel jp, UserAccount account, Organization organization, Enterprise enterprise, Network network,EcoSystem business) {
        return new TeacherSurface(jp, account, (Onlineenterprise)enterprise, (TeacherOrganization)organization,network);
    }
}
