/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.UserAccount;

import Business.Employee.Employee;
import Business.Role.Role;

/**
 *
 * @author qiufeng
 */
public class UserAccount {
       
    private static int count = 100;
    private int accountID;
    private String username;
    private String password;
    private Employee employee;
    private Role role;
    private double time;
    private String path;
    private String Subject;
    
    
        public enum Suject{
        Math,English,Science,Physics,Chineses;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        UserAccount.count = count;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String Subject) {
        this.Subject = Subject;
    }
        
        

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    
    
    public UserAccount(){
        accountID = count;
        count++;

    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }


    
   
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    } 

    public Role getRole() {
        return role;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Employee getEmployee() {
        return employee;
    }
 
    @Override
    public String toString() {
        return String.valueOf(accountID);
    } 
}
