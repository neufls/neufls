/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.UserAccount;

import Business.Employee.Employee;
import Business.Role.Role;
//import com.sun.javafx.sg.prism.NGCanvas;
import java.util.ArrayList;

/**
 *
 * @author qiufeng
 */
public class UserAccountDirectory {
       private ArrayList<UserAccount> userAccountList;

    public UserAccountDirectory() {
        userAccountList = new ArrayList<>();
    }

    public ArrayList<UserAccount> getUserAccountList() {
        return userAccountList;
    }
    
    public UserAccount authenticateUser(String username, String password){
        for (UserAccount ua : userAccountList)
            if (ua.getUsername().equals(username) && ua.getPassword().equals(password)){
                return ua;
            }
        return null;
    }
    
    public UserAccount createUserAccount(String username, String password, Employee employee, Role role,String path, String Subject){
        UserAccount userAccount = new UserAccount();
        userAccount.setUsername(username);
        userAccount.setPassword(password);
        userAccount.setEmployee(employee);
        userAccount.setRole(role);
        userAccount.setPath(path);
        userAccount.setSubject(Subject);
        userAccountList.add(userAccount);
        return userAccount;
    }
    
    public UserAccount findUserAccountByName(String name){
        UserAccount u = null;
        for(UserAccount ua: userAccountList){
            if(ua.getEmployee().getName().equals(name)){
                u = ua;
            }
        }
        return u;
    }
    
    
    public UserAccount findUserAccountBYname2(String name){
        UserAccount u =null;
        for(UserAccount us:userAccountList){
            if(us.getUsername().equals(name)){
                u=us;
            }
        }
        return u;
    }
    
    public boolean checkIfUsernameIsUnique(String username){
        for (UserAccount ua : userAccountList){
            if (ua.getUsername().equals(username))
                return false;
        }
        return true;
    }
    
}
