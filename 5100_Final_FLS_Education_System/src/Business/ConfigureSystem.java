/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Employee.Employee;

import Business.Enterprise.Enterprise;
import Business.Enterprise.Localenterprise;
import Business.Enterprise.Onlineenterprise;
import Business.Network.Network;
import static Business.Organization.Organization.Type.OnlineTA;
import Business.Role.AdminRole;
import Business.Role.LocalTARole;

import Business.Role.OnlineTARole;

import static Business.Role.Role.RoleType.OnlineTA;
import Business.Role.StudentRole;
import Business.Role.TeacherRole;
import Business.UserAccount.UserAccount;
import java.awt.Desktop;

/**
 *
 * @author qiufeng
 */
public class ConfigureSystem {
     
    static String path1="pic/ta1.png";
    static String path3="pic/ta2.png";
    static String path6="pic/ta3.png";
    static String path9="pic/ta4.png";
    
    static String path2="pic/student1.png";
    static String path10="pic/student2.png";
    
   
    static String path4="pic/teacher1.png";
    static String path5="pic/teacher2.png";
    static String path7="pic/teacher3.png";
    static String path8="pic/teacher4.png";
    static String path12="pic/teacher5.png";
 

    public static EcoSystem configure() {
    EcoSystem system = EcoSystem.getInstance();
         
       
//新建一个Network     
        Network us = system.addNetwork("America");
        Network uk = system.addNetwork("United Kingdom");
        
        //LOcal1:
        Localenterprise trans = (Localenterprise) uk.getEnterpriseDirectory().createAndAddEnterprise("Local1", Enterprise.EnterpriseType.Localization);
       
        Employee e = trans.getEmployeeDirectory().createEmployee("LA1");
        UserAccount la1 = trans.getUserAccountDirectory().createUserAccount("la1", "la1", e , new AdminRole(), null ,null);

        Employee Lucky =trans.getEmployeeDirectory().createEmployee("LTA2");
     
        trans.getUserAccountDirectory().createUserAccount("1lt2", "1lt2", Lucky, new LocalTARole(), path9,null);
        
        Employee Lucky2 =trans.getEmployeeDirectory().createEmployee("LTA1");
     
        trans.getUserAccountDirectory().createUserAccount("1lt1", "1lt1", Lucky2, new LocalTARole(), path1,null);
         
        Employee local_p1 = trans.getEmployeeDirectory().createEmployee("Student1");
       
        trans.getUserAccountDirectory().createUserAccount("1s1", "1s1", local_p1, new StudentRole(), path2,null);
        
        Employee local_p2 = trans.getEmployeeDirectory().createEmployee("Student2");
       
        trans.getUserAccountDirectory().createUserAccount("1s2", "1s2", local_p2, new StudentRole(), path10,null);
        
         trans.loadRateInfo();
         
         //Local2
        Localenterprise trans5 = (Localenterprise) uk.getEnterpriseDirectory().createAndAddEnterprise("Local1", Enterprise.EnterpriseType.Localization);
       
        Employee e2 = trans5.getEmployeeDirectory().createEmployee("LA2");
        UserAccount la2 = trans5.getUserAccountDirectory().createUserAccount("la2", "la2", e2 , new AdminRole(), null ,null);

        Employee li =trans5.getEmployeeDirectory().createEmployee("LTA2");
     
        trans5.getUserAccountDirectory().createUserAccount("2lt2", "2lt2", li, new LocalTARole(), path9,null);
        
        Employee li2 =trans5.getEmployeeDirectory().createEmployee("LTA1");
     
        trans5.getUserAccountDirectory().createUserAccount("2lt1", "2lt1", li2, new LocalTARole(), path1,null);
         
        Employee li3 = trans5.getEmployeeDirectory().createEmployee("Student1");
       
        trans5.getUserAccountDirectory().createUserAccount("2s1", "2s1", li3, new StudentRole(), path2,null);
        
        Employee li4= trans5.getEmployeeDirectory().createEmployee("Student2");
       
        trans5.getUserAccountDirectory().createUserAccount("2s2", "2s2", li4, new StudentRole(), path10,null);
        
         trans5.loadRateInfo();
         
         
         
  //一家在线公司   
        Onlineenterprise trans1 = (Onlineenterprise) uk.getEnterpriseDirectory().createAndAddEnterprise("Online1", Enterprise.EnterpriseType.Online);
        Employee at1 = trans1.getEmployeeDirectory().createEmployee("OA1");
        UserAccount ta1 = trans1.getUserAccountDirectory().createUserAccount("oa1", "oa1", at1 , new AdminRole(), null,null);
       
        Employee bb1 =trans1.getEmployeeDirectory().createEmployee("OTA1");
       
        trans1.getUserAccountDirectory().createUserAccount("1ot1", "1ot1", bb1, new OnlineTARole(), path3,null);
         
         
        Employee bb2 =trans1.getEmployeeDirectory().createEmployee("OTA2");
       
        trans1.getUserAccountDirectory().createUserAccount("1ot2", "1ot2", bb2, new OnlineTARole(), path6,null);
         
        Employee bb3 = trans1.getEmployeeDirectory().createEmployee("Teacher1");
//        String path4="./pic/teacher1.png";
        trans1.getUserAccountDirectory().createUserAccount("1t1", "1t1", bb3, new TeacherRole(), path4, "Math");
       
        Employee bb4 = trans1.getEmployeeDirectory().createEmployee("Teacher2");
//        String path4="./pic/teacher1.png";
        trans1.getUserAccountDirectory().createUserAccount("1t2", "1t2", bb4, new TeacherRole(), path5, "English");
       
        Employee bb5 = trans1.getEmployeeDirectory().createEmployee("Teacher3");
//        String path4="./pic/teacher1.png";
        trans1.getUserAccountDirectory().createUserAccount("1t3", "1t3", bb5, new TeacherRole(), path7, "Science");
       
        Employee bb6 = trans1.getEmployeeDirectory().createEmployee("Teacher4");
//        String path4="./pic/teacher1.png";
        trans1.getUserAccountDirectory().createUserAccount("1t4", "1t4", bb6, new TeacherRole(), path8, "Physics");
       
        Employee bb7 = trans1.getEmployeeDirectory().createEmployee("Teacher5");
//        String path4="./pic/teacher1.png";
        trans1.getUserAccountDirectory().createUserAccount("1t5", "1t5", bb7, new TeacherRole(), path12, "Chineses");
       
       
     //第二家在线公司   
   
            Onlineenterprise trans11 = (Onlineenterprise) uk.getEnterpriseDirectory().createAndAddEnterprise("Online2", Enterprise.EnterpriseType.Online);
        Employee at2 = trans11.getEmployeeDirectory().createEmployee("OA2");
        UserAccount ta2 = trans11.getUserAccountDirectory().createUserAccount("oa2", "oa2", at2 , new AdminRole(), null,null);
       
        Employee aa1 =trans11.getEmployeeDirectory().createEmployee("OTA1");
       
        trans11.getUserAccountDirectory().createUserAccount("2ot1", "2ot1", aa1, new OnlineTARole(), path3,null);
         
         
        Employee aa2 =trans11.getEmployeeDirectory().createEmployee("OTA2");
       
        trans11.getUserAccountDirectory().createUserAccount("2ot2", "2ot2", aa2, new OnlineTARole(), path6,null);
         
        Employee aa3 = trans11.getEmployeeDirectory().createEmployee("Teacher1");
//        String path4="./pic/teacher1.png";
        trans11.getUserAccountDirectory().createUserAccount("2t1", "2t1", aa3, new TeacherRole(), path4, "Math");
       
        Employee aa4 = trans11.getEmployeeDirectory().createEmployee("Teacher2");
//        String path4="./pic/teacher1.png";
        trans11.getUserAccountDirectory().createUserAccount("2t2", "2t2", aa4, new TeacherRole(), path5, "English");
       
        Employee aa5 = trans11.getEmployeeDirectory().createEmployee("Teacher3");
//        String path4="./pic/teacher1.png";
        trans11.getUserAccountDirectory().createUserAccount("2t3", "2t3", aa5, new TeacherRole(), path7, "Science");
       
        Employee aa6 = trans11.getEmployeeDirectory().createEmployee("Teacher4");
//        String path4="./pic/teacher1.png";
        trans11.getUserAccountDirectory().createUserAccount("2t4", "2t4", aa6, new TeacherRole(), path8, "Physics");
       
        Employee aa7 = trans11.getEmployeeDirectory().createEmployee("Teacher5");
//        String path4="./pic/teacher1.png";
        trans11.getUserAccountDirectory().createUserAccount("2t5", "2t5", aa7, new TeacherRole(), path12, "Chineses");
       
       
       
     return system;
}
}

