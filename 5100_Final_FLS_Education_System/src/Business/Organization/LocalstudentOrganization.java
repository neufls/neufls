/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.Role;
import Business.Role.StudentRole;
import java.util.ArrayList;

/**
 *
 * @author qiufeng
 */
public class LocalstudentOrganization extends Organization{
        
    public LocalstudentOrganization() {
        super(Type.LocalStudent.getValue());
        
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new StudentRole());
        return roles;
    }
}
