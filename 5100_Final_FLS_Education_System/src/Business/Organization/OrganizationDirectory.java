/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import java.util.ArrayList;

/**
 *
 * @author qiufeng
 */
public class OrganizationDirectory {
     private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList<>();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }

    public Organization createOrganization(Organization.Type type) {
        Organization organization = null;
     
        if (type.getValue().equals(Organization.Type.OnlineTA.getValue())) {
            organization = new OnlineTAOrganization();
            organizationList.add(organization);
        }
        if (type.getValue().equals(Organization.Type.Teacher.getValue())) {
            organization = new TeacherOrganization();
            organizationList.add(organization);
        }
        if (type.getValue().equals(Organization.Type.LocalStudent.getValue())) {
            organization = new LocalstudentOrganization();
            organizationList.add(organization);
        }
        if (type.getValue().equals(Organization.Type.LocalTA.getValue())) {
            organization = new LocalTAOrganization();
            organizationList.add(organization);
        }
        return organization;
    }  
}
