/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.OnlineTARole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author qiufeng
 */
public class OnlineTAOrganization extends Organization{
      //  private static int id = 1;
    public OnlineTAOrganization() {
        super(Organization.Type.OnlineTA.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new OnlineTARole());
        return roles;
    }
}
