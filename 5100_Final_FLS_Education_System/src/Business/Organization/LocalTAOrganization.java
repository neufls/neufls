/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.LocalTARole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author qiufeng
 */
public class LocalTAOrganization extends Organization{
        public LocalTAOrganization() {
        super(Organization.Type.LocalTA.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new LocalTARole());
        return roles;
}
}