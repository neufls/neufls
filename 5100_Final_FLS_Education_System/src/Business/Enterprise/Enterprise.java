/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

//import Business.Lession.LessionDirectory;
import Business.Organization.Organization;
import Business.Organization.OrganizationDirectory;
import Business.Profit.ProfitDirectory;
import Business.RateInfo.RateDirectory;
import Business.RateInfo.RateInfo;
import java.io.BufferedReader;
import java.io.FileReader;

/**
 *
 * @author qiufeng
 */
public abstract class Enterprise extends Organization{
    private EnterpriseType enterpriseType;
    private OrganizationDirectory organizationDirectory;
     ProfitDirectory profitDirector;
     RateDirectory rateDirectory;
 //    LessionDirectory LessoinDir;
     
    public Enterprise(String name, EnterpriseType type) {
        super(name);
        this.enterpriseType = type;
        organizationDirectory = new OrganizationDirectory();
       profitDirector=new ProfitDirectory();
       rateDirectory = new RateDirectory();
 //      LessoinDir=new LessionDirectory();
    }

   
    
//
   
      public ProfitDirectory getProfitDirector() {
        return profitDirector;
    }

    public void setProfitDirector(ProfitDirectory p) {
        this.profitDirector = p;
    }

    
    public enum EnterpriseType{
        Localization("Localization Company"),Online("Online Company");
        
        private String value;

        private EnterpriseType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    public EnterpriseType getEnterpriseType() {
        return enterpriseType;
    }

    public OrganizationDirectory getOrganizationDirectory() {
        return organizationDirectory;
    }
    
    public RateDirectory getRateDirectory() {
        return rateDirectory;
    }

    public void setRateDirectory(RateDirectory rateDirectory) {
        this.rateDirectory = rateDirectory;
    }
    
    public void loadRateInfo() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("Data_csv/LocalSupplier.csv"));
            String line = null;
            while ((line = reader.readLine()) != null) {
                String Item[] = line.split(",");

                RateInfo si = this.getRateDirectory().createSupplierInfo();
                si.setEnterprise(Item[0]);
                si.setRate(Integer.parseInt(Item[1]));

                System.out.println(this.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
}
