/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import java.util.ArrayList;

/**
 *
 * @author qiufeng
 */
public class EnterpriseDirectory {
        private ArrayList<Enterprise> enterpriseList;

    public EnterpriseDirectory() {
        enterpriseList = new ArrayList<>();
    }

    public ArrayList<Enterprise> getEnterpriseList() {
        return enterpriseList;
    }

    public Enterprise createAndAddEnterprise(String name, Enterprise.EnterpriseType type) {
        Enterprise enterprise = null;
        if (type == Enterprise.EnterpriseType.Localization) {
            enterprise = new Localenterprise(name);
            enterpriseList.add(enterprise);
        }
        if (type == Enterprise.EnterpriseType.Online) {
            enterprise = new Onlineenterprise(name);
            enterpriseList.add(enterprise);
        }
        return enterprise;
    }
    
    public Onlineenterprise findOnlineCompanyByName(String name){
        Onlineenterprise te = null;
        for(Enterprise e: enterpriseList){
            if(e.getClass().getSimpleName().equals("OnlineEnterprise") && e.getName().equals(name)){
                te = (Onlineenterprise) e;
            }
        
        }
        
        
        return te;
    }
}
