/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.RateInfo;

import java.util.ArrayList;

/**
 *
 * @author LZN
 */
public class RateDirectory {
    ArrayList<RateInfo> schoollist;

    public RateDirectory() {
        schoollist = new ArrayList<>();
    }

    public ArrayList<RateInfo> getSchoollist() {
        return schoollist;
    }

    public void setSchoollist(ArrayList<RateInfo> schoollist) {
        this.schoollist = schoollist;
    }
    
    public RateInfo createSupplierInfo(){
        RateInfo si = new RateInfo();
        schoollist.add(si);
        return si;
    }
      
    public RateInfo findInfoByName(String name){
        RateInfo supplierInfo = null;
        System.out.println("!!!!!!!!!");
        for(RateInfo si: schoollist){
            System.out.println(si+"***********");
            if(si.getEnterprise().equals(name))
                supplierInfo = si;
        }
        System.out.println(supplierInfo+"@@@@@");
        return supplierInfo;   
    }
}
