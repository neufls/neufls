/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Profit;

/**
 *
 * @author qiufeng
 */
public class Profit {
    private String teachername;
    private double profit;
    
    public Profit(){
        
    }
    
    public Profit(String teachername,double profit){
        this.teachername = teachername;
        this.profit = profit;
    }

    public String getTeachername() {
        return teachername;
    }

    public void setTeachername(String teachername) {
        this.teachername = teachername;
    }

    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }
    @Override
        public String toString() {
            return getTeachername();
        }
    
    
    
    
    
}
