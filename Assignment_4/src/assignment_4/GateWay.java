/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author harshalneelkamal
 */
public class GateWay {
    
    public static void main(String args[]) throws IOException{
        /*
        DataGenerator generator = DataGenerator.getInstance();
        
        //Below is the sample for how you can use reader. you might wanna 
        //delete it once you understood.
        
        DataReader orderReader = new DataReader(generator.getOrderFilePath());
        String[] orderRow;
        printRow(orderReader.getFileHeader());
        while((orderRow = orderReader.getNextRow()) != null){
            printRow(orderRow);
        }
        System.out.println("_____________________________________________________________");
        DataReader productReader = new DataReader(generator.getProductCataloguePath());
        String[] prodRow;
        printRow(productReader.getFileHeader());
        while((prodRow = productReader.getNextRow()) != null){
            printRow(prodRow);
        }
        */
        
        
<<<<<<< HEAD
        
=======
        Top3PopularProducts();
>>>>>>> remotes/origin/DezhiYang1
        threeBestSalesPeople();
        totalRevenue();
    }
    
    public static void printRow(String[] row){
        for (String row1 : row) {
            System.out.print(row1 + ", ");
        }
        System.out.println("");
    }
    
    public static void threeBestSalesPeople()throws IOException{
        DataGenerator generator = DataGenerator.getInstance();       
        DataReader productReader = new DataReader(generator.getOrderFilePath());
        String[] row;
        HashMap<Integer,Integer> bestSales = new HashMap<>();
        
        //put key-value pairs into map (sellerID - total price)
        while((row = productReader.getNextRow()) != null){
            int quantity = Integer.parseInt(row[3]);
            int salesID = Integer.parseInt(row[4]);
            int prodPrice = Integer.parseInt(row[6]);
            int total = quantity * prodPrice;
            
            if(bestSales.containsKey(salesID))
                total += bestSales.get(salesID);
            bestSales.put(salesID, total);            
        }
        
        //sort with desc order, override compare method .     
        List<Integer> sales = new ArrayList<>(bestSales.values());
        Collections.sort(sales, new Comparator<Integer>() {
            @Override
            public int compare(Integer i1, Integer i2) {
                return i2 - i1;
            }
        }); 
        
        //consider different sellers have the same total selling price.
        //record the total price value.
        int[] countNum = new int[3];
        int j=0;
        for(int i=0;i<sales.size() && j<3;i++){
            if(i==0 || sales.get(i)!=countNum[j])
                countNum[j++] = sales.get(i);
        }
        //record the sellerID with corresponding price value.
        int[] salesID = new int[3];
        for(int id  : bestSales.keySet()){
            if(bestSales.get(id)==countNum[0]) salesID[0]=id;
            else if(bestSales.get(id)==countNum[1]) salesID[1]=id;
            else if(bestSales.get(id)==countNum[2]) salesID[2]=id;
        }
        
        System.out.println("Top 3 best sales people: "); 
        for(int i=0;i<3;i++)
            System.out.println("SalesID: "+salesID[i]+", "+"total: "+countNum[i]);
        System.out.println();  
        }                                           
    
    public static void totalRevenue() throws IOException{
        int total=0;
        DataGenerator generator = DataGenerator.getInstance();       
        DataReader productReader = new DataReader(generator.getOrderFilePath());
        DataReader minpriceReader = new DataReader(generator.getProductCataloguePath());
        String[] prodRow;
        String[] minpriceRow;
        
        //read file and put key-value pairs into map
        HashMap<Integer,Integer> minprice = new HashMap<>();
        while((minpriceRow = minpriceReader.getNextRow()) != null){
            minprice.put(Integer.parseInt(minpriceRow[0]), Integer.parseInt(minpriceRow[1]));
        }
        
        //calculate the totle revenue with quantity*price
        while((prodRow = productReader.getNextRow()) != null){
            int priceDiff = Integer.parseInt(prodRow[6]) - minprice.get(Integer.parseInt(prodRow[2]));
            int quantity = Integer.parseInt(prodRow[3]);
            int moneyPerOrder = quantity * priceDiff;
            
            total += moneyPerOrder;
        }                                           
        
        System.out.println("Total Revenue For the Year: "+total); 
    }
<<<<<<< HEAD
=======
    public static void Top3PopularProducts() throws IOException{
                
        DataGenerator generator = DataGenerator.getInstance();

        DataReader orderReader = new DataReader(generator.getOrderFilePath());
        String[] orderRow;
        
        
        HashMap<Integer,Integer> MostPopProduct= new HashMap<>();
        
        while ((orderRow=orderReader.getNextRow())!=null) {
            int quantity = Integer.parseInt(orderRow[3]);
            int prodId = Integer.parseInt(orderRow[2]);
            if(MostPopProduct.containsKey(prodId))
                quantity += MostPopProduct.get(prodId);
            MostPopProduct.put(prodId, quantity);
        }
        
         List<Integer> quantityCountList = new ArrayList<>(MostPopProduct.values());
        
        Collections.sort(quantityCountList, new Comparator<Integer>() {
            @Override
            public int compare(Integer i1, Integer i2) {
                //so as to get ascending list
                return i2 - i1;
            }
        });

        int[] countNum = new int[3];
        int j=0;
        for(int i=0;i<quantityCountList.size() && j<3;i++){
            if(i==0 || quantityCountList.get(i)!=countNum[j])
                countNum[j++] = quantityCountList.get(i);
        }
        
        int[] prodID = new int[3];
        for(int id  : MostPopProduct.keySet()){
            if(MostPopProduct.get(id)==countNum[0]) prodID[0]=id;
            else if(MostPopProduct.get(id)==countNum[1]) prodID[1]=id;
            else if(MostPopProduct.get(id)==countNum[2]) prodID[2]=id;
        }
        
        System.out.println("Top 3 of the most popular product from high to low: "); 
        for(int i=0;i<3;i++)
            System.out.println("ProductID:"+prodID[i]+","+"Quantity:"+countNum[i]);
                System.out.println("'Ordered by saled quantities of products from high to low'"); 

        System.out.println();  
    }
>>>>>>> remotes/origin/DezhiYang1
}
