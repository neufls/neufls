/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab_8.analytics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import lab_8.entities.Comment;
import lab_8.entities.Post;
import lab_8.entities.User;

/**
 *
 * @author harshalneelkamal
 */
public class AnalysisHelper {
    
    
    public void userWithMostLikes(){
        Map<Integer,Integer> userLikecount = new HashMap<Integer,Integer>();
        
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        for(User user :users.values()){
            for(Comment c:user.getComments()){
                int likes=0;
                if(userLikecount.containsKey(user.getId()))
                    likes=userLikecount.get(user.getId());
                likes+=c.getLikes();
                userLikecount.put(user.getId(), likes);
            }
        }
        int max=0;
        int maxId=0;
        for(int id :userLikecount.keySet()){
            if(userLikecount.get(id)>max){
                max=userLikecount.get(id);
                maxId=id;
            }
        }
        System.out.println("User with most Likes:"+max+"\n"+users.get(maxId));
        
    }
    
    public void getFiveMostLikedComment(){
        Map<Integer,Comment> comments=DataStore.getInstance().getComments();
        
        List<Comment> commentList= new ArrayList<>(comments.values());
        
        Collections.sort(commentList,new Comparator<Comment>() {
        @Override
        public int compare(Comment o1,Comment o2){
            // so as to get decending list
            return o2.getLikes() - o1.getLikes();
        }
    });
        
           System.out.println("***5 most liked comments: ");
        for(int i=0; i<commentList.size() && i<5; i++){
            System.out.println(commentList.get(i));
        }
        System.out.println();  
    }
    
    
    
    public void getAvaLikeofC(){
        int likes =0;

//        Map<Integer,Integer> userLikecount = new HashMap<Integer,Integer>();
//        Map<Integer, User> users = DataStore.getInstance().getUsers();
//
//        for(User user :users.values()){
//
//            for(Comment c:user.getComments()){
//
//                if(userLikecount.containsKey(user.getId()))
//                   likes=userLikecount.get(user.getId());
//                likes+=c.getLikes();
//              userLikecount.put(user.getId(), likes);
//            }

            Map<Integer,Comment> comments=DataStore.getInstance().getComments();
            List<Comment> commentList= new ArrayList<>(comments.values());

            for(Comment c:comments.values()){
                likes+=c.getLikes();
            }
            
        System.out.println("***Average number of likes per comment: "+(likes/commentList.size()));

        System.out.println(); 
         
    }
    
    public void Postwithmostlikedcomments(){
        Map<Integer,Comment> comments=DataStore.getInstance().getComments();
        int maxlike =0;
        int maxId=0;
        
        for(Comment c:comments.values()){
             if(c.getLikes()>maxlike){
                 maxlike=c.getLikes();
                 maxId=c.getPostId();
             }
            }
            System.out.println("Post with most liked comments:"+maxId);
        
    }
    
    
    
    
    
    
    
    
    
    public void Postwithmostcomments(){
        Map<Integer,Integer> postcommitcount =new HashMap<Integer,Integer>();
        Map<Integer,Comment> comments=DataStore.getInstance().getComments();
        
       for(Comment c:comments.values()){
           
           int postnum=0;
           
                if(postcommitcount.containsKey(c.getPostId()))
                postnum=postcommitcount.get(c.getPostId());
                
                postnum++;
                
                postcommitcount.put(c.getPostId(), postnum);
           
       }
       
          int max=0;
          int maxId=0;
          for(int id :postcommitcount.keySet()){
            if(postcommitcount.get(id)>max){
                max=postcommitcount.get(id);
                maxId=id;
            }
        }
        System.out.println("Post with most comments:"+maxId);
    }
    
     public void userWithLeastPosts(){
        Map<Integer, Integer> userPosts = createUserPostMap();
        
        List<Integer> postCountList = new ArrayList<>(userPosts.values());
        
        Collections.sort(postCountList, new Comparator<Integer>() {
            @Override
            public int compare(Integer i1, Integer i2) {
                //so as to get ascending list
                return i1 - i2;
            }
        });
        
        //Use LinkedHashSet if want to preserve order and remove duplicate post counts
        Set<Integer> unique = new LinkedHashSet<>(postCountList);
        postCountList = new ArrayList<>(unique);
        
        System.out.println("***5 most inactive users (with least posts): ");
        int flag = 0;
        for(int i=0; i<postCountList.size() && i<5; i++){
            int count = postCountList.get(i);
            for(int userId : userPosts.keySet()){
                if(userPosts.get(userId) == count){
                    flag++;
                    if(flag <= 5)
                        System.out.println("User ID: " + userId + ", post count: " + count);
                    else break;   
                }   
            }   
        }
        System.out.println();    
        
    }
    
    //Top 5 inactive users based on comments
    public void userWithLeastComments(){
        Map<Integer,Integer> userComments = createUserCommentsMap();
        
        List<Integer> commCountList = new ArrayList<>(userComments.values());
        
        Collections.sort(commCountList, new Comparator<Integer>() {
            @Override
            public int compare(Integer i1, Integer i2) {
                //so as to get ascending list
                return i1 - i2;
            }
        });
        //System.out.println("1--->"+commCountList);
        
        //Use LinkedHashSet if want to preserve order and remove duplicate comment counts
        Set<Integer> unique = new LinkedHashSet<>(commCountList);
        commCountList = new ArrayList<>(unique);
        //System.out.println("2--->"+commCountList);
        
        System.out.println("***5 most inactive users (with least comments): ");
        int flag = 0;
        for(int i=0; i< commCountList.size() && i<5; i++){
            int count = commCountList.get(i);
            for(int userId : userComments.keySet()){
                if(userComments.get(userId) == count){
                    flag++;
                    if(flag <= 5)
                        System.out.println("User ID: " + userId + ", comment count: " + count);
                    else break;   
                }
                    
            }   
        }
        System.out.println();    
    }
    
    //Top 5 inactive users overall (comments, posts)
    public void inactiveUsersOverall(){
        Map<Integer,Integer> userPosts = createUserPostMap();
        Map<Integer,Integer> userComments = createUserCommentsMap();
        Map<Integer, Integer> userAll = createUserAllMap(userPosts, userComments);
        
        List<Integer> totalCountList = new ArrayList<>(userAll.values());
        
        Collections.sort(totalCountList, new Comparator<Integer>() {
            @Override
            public int compare(Integer i1, Integer i2) {
                //so as to get ascending list
                return i1 - i2;
            }
        });
        
        Set<Integer> unique = new LinkedHashSet<>(totalCountList);
        totalCountList = new ArrayList<>(unique);
        
        System.out.println("***5 most inactive users (considering post count and comment count): ");
        int flag = 0;
        for(int i=0; i< totalCountList.size() && i<5; i++){
            int count = totalCountList.get(i);
            for(int userId : userAll.keySet()){
                if(userAll.get(userId) == count){
                    flag++;
                    if(flag <= 5)
                         System.out.println("User ID: " + userId + ", post count: " + userPosts.get(userId) + 
                                            ", comment count: " + userComments.get(userId) + ", total count: " + count);
                         
                    else break;
                }     
            }   
        }
        
        
        System.out.println();    
        
    }
    
    
    //Top 5 proactive users overall (comments, posts)
    public void proactiveUsersOverall(){
        Map<Integer,Integer> userPosts = createUserPostMap();
        Map<Integer,Integer> userComments = createUserCommentsMap();
        Map<Integer, Integer> userAll = createUserAllMap(userPosts, userComments);
        
        List<Integer> totalCountList = new ArrayList<>(userAll.values());
        
        Collections.sort(totalCountList, new Comparator<Integer>() {
            @Override
            public int compare(Integer i1, Integer i2) {
                //so as to get ascending list
                return i2 - i1;
            }
        });
        
        Set<Integer> unique = new LinkedHashSet<>(totalCountList);
        totalCountList = new ArrayList<>(unique);
        
        System.out.println("***5 most proactive users (considering post count and comment count): ");
        int flag = 0;
        for(int i=0; i<totalCountList.size() && i<5; i++){
            int count = totalCountList.get(i);
            for(int userId : userAll.keySet()){
                if(userAll.get(userId) == count){
                    flag++;
                    if(flag <= 5)
                        System.out.println("User ID: " + userId + ", post count: " + userPosts.get(userId) + 
                                            ", comment count: " + userComments.get(userId) + ", total count: " + count);
                    else break;
                }    
            }   
        }
        
        System.out.println();    
    }

    //222222222222222222222222222222222222
    public Map createUserPostMap(){
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        
        //key is user ID and value is post count of the user
        Map<Integer, Integer> userPosts = new HashMap<Integer, Integer>();
        
        for(Post p : posts.values()){
            int postCount = 1;
            if(userPosts.containsKey(p.getUserId()))
                postCount = userPosts.get(p.getUserId()) + 1;
            userPosts.put(p.getUserId(), postCount);
        }
        return userPosts;
    }
    
    public Map createUserCommentsMap(){
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
        
        //key is user ID and value is comment count of the user
        Map<Integer, Integer> userComments = new HashMap<Integer, Integer>();
        
         for(Comment c : comments.values()){
            int commentCount = 1;
            if(userComments.containsKey(c.getUserId()))
                commentCount = userComments.get(c.getUserId()) + 1;
            userComments.put(c.getUserId(), commentCount);
        }
        return userComments;
    }
    
    public Map createUserAllMap(Map<Integer,Integer> userPosts, Map<Integer,Integer> userComments){
        Map<Integer,Integer> userAll = new HashMap<>();
        
        for (int userId : userPosts.keySet()){
            int totalCount = userPosts.get(userId) + userComments.get(userId);
            userAll.put(userId, totalCount);
        }
        
        return userAll;
    }
    















public void Top5inactiveusersbasedonposts(){
        Map<Integer,Integer> postcount =new HashMap<Integer,Integer>();
        Map<Integer,Comment> comments=DataStore.getInstance().getComments();
        
        for(Comment c:comments.values()){
            int postnum=0;
                if(postcount.containsKey(c.getPostId()))
                postnum=postcount.get(c.getPostId());
                postnum++;
                
                postcount.put(c.getPostId(), postnum);
}
        Map<Integer, User> users = DataStore.getInstance().getUsers();
        Map<Integer,Integer> usercount =new HashMap<Integer,Integer>();
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        //Map<Integer,Integer> postcount =new HashMap<Integer,Integer>();
        
        for(Post post:posts.values()){
           int userId = post.getUserId();
                if(postcount.containsKey(userId)){
                    postcount.put(userId, postcount.get(userId) + 1);
                }else{
                    postcount.put(userId, 1);
                }
            
            }
        
            //sort
            List<Integer> postList= new ArrayList<>(postcount.values());
        
            Collections.sort(postList,new Comparator<Integer>() {
                @Override
                public int compare(Integer o1,Integer o2){
                    // so as to get decending list
                    return o1-  o2;
                }
            });
        
        
        
            System.out.println("***5 most inactive: ");
            for(int i=0; i<postList.size() && i<5; i++){
                System.out.println(postList.get(i));
            }
            System.out.println();  

            List<Comment> commentList= new ArrayList<>(comments.values());
            Collections.sort(commentList,new Comparator<Comment>() {
                @Override
                public int compare(Comment o1,Comment o2){
                    // so as to get decending list
                    return o2.getLikes() - o1.getLikes();
                }
            });
        }
    }

        





















